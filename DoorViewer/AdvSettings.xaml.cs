﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoorViewer
{
    /// <summary>
    /// Interaktionslogik für AdvSettings.xaml
    /// </summary>
    public partial class AdvSettings : Window
    {
        public AdvSettings()
        {
            InitializeComponent();
            loadAdvSettings();
            textbox_ipcurl.TextChanged += new TextChangedEventHandler(ipc_textChange);
        }
        private void loadAdvSettings()
        {
           textbox_multiip.Text = Properties.Settings.Default.muticast;
           textbox_port.Text = Properties.Settings.Default.multicast_port;
           textbox_ipcurl.Text = Properties.Settings.Default.ipc_stream;
            //Check If IPC Stream URl is there, if so user can select IPC Cam to show on Ring 
            if (textbox_ipcurl.Text.Trim().Equals("")){
                label_ringcam.IsEnabled = false;
                radio_door.IsEnabled = false;
                radio_ipc.IsEnabled = false;
            }
            else{
                label_ringcam.IsEnabled = true;
                radio_door.IsEnabled = true;
                radio_ipc.IsEnabled = true;
            }
        }
        private void ipc_textChange(object Sender, TextChangedEventArgs e)
        {
            if(textbox_ipcurl.Text.Trim().Equals("")){
                label_ringcam.IsEnabled = false;
                radio_door.IsEnabled = false;
                radio_ipc.IsEnabled = false;
            }
            else{
                label_ringcam.IsEnabled = true;
                radio_door.IsEnabled = true;
                radio_ipc.IsEnabled = true;
            }
        }
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
        void Button_Save_ADV_Click(object sender, EventArgs e)
        {

            var multiip = textbox_multiip.Text.Trim();
            var match_multiip = Regex.Match(multiip, @"\b(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b");
            if (!match_multiip.Success)
            {
                textbox_multiip.Foreground = Brushes.Red;
            }

            var ipcURL = textbox_ipcurl.Text.Trim();
            var match_ipc = true;
            if (!ipcURL.Equals(""))
            {
               match_ipc = Regex.Match(ipcURL, @"\b(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b").Success;
            }

            if (!match_ipc)
            {
                textbox_ipcurl.Foreground = Brushes.Red;
            }

            //Save ADV Settings
            if (match_multiip.Success && match_ipc)
            {
                Properties.Settings.Default.muticast = textbox_multiip.Text.Trim();
                Properties.Settings.Default.multicast_port = textbox_port.Text.Trim();
                Properties.Settings.Default.ipc_stream = textbox_ipcurl.Text.Trim();

                if (radio_door.IsChecked == true)
                {
                    Properties.Settings.Default.camShow = "DOOR";
                }
                else
                {
                    Properties.Settings.Default.camShow = "IPC";
                }

                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();

                MainWindow.checkSystemTray();

                this.Close();
               
            }
            else
            {
                SystemSounds.Exclamation.Play();
            }

            }
    }
}
