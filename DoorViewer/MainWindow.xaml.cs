﻿using Notifications.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoorViewer
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Windows.Forms.NotifyIcon notifyIcon = null;
        static System.Windows.Forms.ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();


        static System.Windows.Forms.MenuItem menuItemDoor = new System.Windows.Forms.MenuItem();
        static System.Windows.Forms.MenuItem menuItemIPC = new System.Windows.Forms.MenuItem();
        static System.Windows.Forms.MenuItem menuItemSettings = new System.Windows.Forms.MenuItem();
        static System.Windows.Forms.MenuItem menuItemClose = new System.Windows.Forms.MenuItem();
        //MainWindow background when Running

        public MainWindow()
        {
           if (Properties.Settings.Default.first_startup)
            {
                Settings window_Settings = Settings.getSettingsWindow();
                if(window_Settings != null)
                {
                    window_Settings.Show();
                }
            }
            InitializeComponent();
            setupSystemTray();
            bell_listener bell = bell_listener.Instance;
            this.Hide();


        }

        public static void checkSystemTray()
        {
            if (Properties.Settings.Default.ipc_stream == "")
            {
                if (contextMenu.MenuItems.Contains(menuItemIPC))
                {
                    contextMenu.MenuItems.Remove(menuItemIPC);
                }
            }
            else
            {
                if (!contextMenu.MenuItems.Contains(menuItemIPC))
                {
                    contextMenu.MenuItems.Add(menuItemIPC);
                }
            }
            if (Properties.Settings.Default.username == "")
            {
                if (contextMenu.MenuItems.Contains(menuItemDoor))
                {
                    contextMenu.MenuItems.Remove(menuItemDoor);
                }
            }
            else
            {
                if (!contextMenu.MenuItems.Contains(menuItemDoor))
                {
                    contextMenu.MenuItems.Add(menuItemDoor);
                }
            }
        }
        public void setupSystemTray()
        {

            menuItemDoor.Text = "&Door";
            menuItemIPC.Text = "&IPC";
            menuItemSettings.Text = "&Settings";
            menuItemClose.Text = "&Close";

            if (Properties.Settings.Default.username != "" && Properties.Settings.Default.password != "" && Properties.Settings.Default.main_ip != "")
            {
                contextMenu.MenuItems.Add(menuItemDoor);
            }

            if (Properties.Settings.Default.ipc_stream != "")
            {
                contextMenu.MenuItems.Add(menuItemIPC);
            }
            contextMenu.MenuItems.Add(menuItemSettings);
            contextMenu.MenuItems.Add(menuItemClose);

            menuItemDoor.Click += new System.EventHandler(this.menuItemDoor_Click);
            menuItemIPC.Click += new System.EventHandler(this.menuItemIPC_Click);
            menuItemSettings.Click += new System.EventHandler(this.menuItemSettings_Click);
            menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);


            notifyIcon = new System.Windows.Forms.NotifyIcon(); 
            notifyIcon.Icon = new Icon(Application.GetResourceStream(new Uri("pack://application:,,,/Resources/icon.ico")).Stream);
            notifyIcon.ContextMenu = contextMenu;
            notifyIcon.Visible = true;
            
        }
        void menuItemDoor_Click(object sender, EventArgs e)
        {
            camViewer window_camViewer = camViewer.camViewerGetWindow("DOOR", false);
            if(window_camViewer != null)
            {
                window_camViewer.Show();
            }
        }
        void menuItemIPC_Click(object sender, EventArgs e)
        {
            camViewer window_camViewer = camViewer.camViewerGetWindow("IPC", false);
            if (window_camViewer != null)
            {
                window_camViewer.Show();
            }
        }
        void menuItemSettings_Click(object sender, EventArgs e)
        {
            Settings window_Settings = Settings.getSettingsWindow();
            if (window_Settings != null)
            {
                window_Settings.Show();
            }
        }
        void menuItemClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
       
    }
}
