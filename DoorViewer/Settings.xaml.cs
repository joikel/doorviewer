﻿using Notifications.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoorViewer
{
    /// <summary>
    /// Interaktionslogik für Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        NotificationManager notificationManager = new NotificationManager();

        private static bool isOpen = false;
       
        public static Settings getSettingsWindow()
        {
            if (isOpen)
            {
                return null;
            }
            else
            {
                isOpen = true;
                return new Settings();
            }
        }
        private Settings()
        {   
            InitializeComponent();
            loadSettings();
            SaveSettings();
        }
     
        public void loadSettings()
        {
            field_username.Text = Properties.Settings.Default.username;
            field_password.Password = Properties.Settings.Default.password;
            field_ip.Text = Properties.Settings.Default.main_ip;
        }

        public void SaveSettings()
        {
          
        }
        
        void Button_Save_Click(object sender, EventArgs e)
        {
            //Check if IP Field Contais valid IP
            var ip = field_ip.Text.Trim();
            var match = Regex.Match(ip, @"\b(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b");
            //Save All Settings and show Notification and Close Window
            if (match.Success)
            {
                Properties.Settings.Default.main_ip = ip;
                Properties.Settings.Default.username = field_username.Text.Trim();
                Properties.Settings.Default.password = field_password.Password.Trim();

                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();

                notificationManager.Show(new NotificationContent
                {
                    Title = "Saved",
                    Message = "Your Settings have been saved!",
                    Type = NotificationType.Success
                });

                MainWindow.checkSystemTray();
                this.Close();
            }
            //Alert Sound and Highlight Red
            else
            {
                SystemSounds.Exclamation.Play();
                field_ip.BorderBrush = Brushes.Red;
                label_ip.Foreground = Brushes.Red;
            }
        }
        
        protected override void OnClosing(CancelEventArgs e)
        {
            isOpen = false;
            System.GC.Collect();
            if (Properties.Settings.Default.username.Equals(""))
            {
                notificationManager.Show(new NotificationContent
                {
                    Title = "Error",
                    Message = "VTO Infomation is empty!",
                    Type = NotificationType.Error
                });
            }else if (Properties.Settings.Default.first_startup)
            {
                notificationManager.Show(new NotificationContent
                {
                    Title = "System Tray",
                    Message = "DoorViewer can now be found in your System Tray",
                    Type = NotificationType.Success
                });
                Properties.Settings.Default.first_startup = false;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
            }
        }
        void Button_AdvSettings_Click(object sender, EventArgs e)
        {
            AdvSettings window_advSettings = new AdvSettings();
            window_advSettings.ShowInTaskbar = false;
            window_advSettings.Owner = this;
            window_advSettings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window_advSettings.ShowDialog();
        }
    }
}
